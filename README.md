# Getting the Source #

`git clone --recurse-submodules https://bitbucket.org/Xaleros/openue.git`

# Building #

## Linux ##

TODO

## Windows ##

### CMake (Visual Studio Only) ###

TODO

### CMake (command line) ###

1. Install CMake 3.18 from https://cmake.org/download/
2. Download required dependencies
    1. Download libopenmpt from https://lib.openmpt.org/libopenmpt/download/
    2. Download OpenAL "Core SDK" from http://openal.org/downloads/ or download and build openal-soft from https://openal-soft.org/#download
3. Download optional (recommended) dependencies
    1. Download SDL2 "Development Libraries" from https://www.libsdl.org/download-2.0.php
4. Install dependencies
    1. Extract libopenmpt to a directory, and note the full path to that directory
    2. Install OpenAL "Core SDK" or build and install openal-soft
    3. (Optional/recommended) Extract SDL2 to a directory and note the full path to that directory
5. Create `build` and `install` directories
6. Open a terminal and change into the `build` directory
7. Invoke CMake, provide paths to dependencies  
For instance `cmake -DCMAKE_PREFIX_PATH="C:/Program Files (x86)/OpenAL 1.1 SDK;C:/Users/you/path/to/libopenmpt;C:/Users/you/path/to/SDL2" -DCMAKE_INSTALL_PREFIX=C:/Users/you/pathh/to/install/directory -DLIBUNR_BUILD_EDITOR=0`
Note that CMake automatically will convert forward slashes into windows-compatible backslashes, which simplifies quoting and escaping.
`CMAKE_PREFIX_PATH` is a semicolon separated list of paths where CMake should look for dependencies, so it's important you provide all of them.
`LIBUNR_BUILD_EDITOR=0` disables building the editor with CMake as this is currently broken.

8. Build  
`cmake --build .`

9. Install  
`cmake --install .`

10. Test  
TODO
